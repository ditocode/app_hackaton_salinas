import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageSate createState() => _LoginPageSate();
}

class _LoginPageSate extends State<LoginPage> {
  String _email;
  String _password;
  //google sign
  final formkey = new GlobalKey<FormState>();
  checkFields() {
    final form = formkey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  LoginUser() {
    if (checkFields()) {
      FirebaseAuth.instance
          .signInWithEmailAndPassword(email: _email, password: _password)
          .then((user) {
        Navigator.of(context).pushReplacementNamed('/userpage');
      }).catchError((e) {
        print(e);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
      appBar: AppBar(
        title: Image(
          image: AssetImage(
            "images/logo.png",
          ),
          height: 200.0,
          fit: BoxFit.fitHeight,
        ),
        elevation: 0.0,
        centerTitle: true,
        backgroundColor: Colors.transparent,
      ),
      body: ListView(
        shrinkWrap: true,
        children: <Widget>[
          Container(
            height: 220.0,
            width: 110.0,
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('images/monkey.gif'), fit: BoxFit.cover),
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(500.0),
                  bottomRight: Radius.circular(500.0)),
            ),
          ),
          Center(
            child: Padding(
              padding: const EdgeInsets.all(28.0),
              child: Center(
                  child: Form(
                key: formkey,
                child: Center(
                  child: ListView(
                    shrinkWrap: true,
                    children: <Widget>[
                      _input("Captura tu correo.", false, "Correo electrónico",
                          'Ingresa tu Correo electrónico', (value) => _email = value,"prueba@prueba.com"),
                      SizedBox(
                        width: 20.0,
                        height: 20.0,
                      ),
                      _input("Captura tu contraseña", true, "Contaseña", 'Contraseña',
                          (value) => _password = value,"hola1234"),
                      new Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Center(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: OutlineButton(
                                          child: Text("Ingresar"),
                                          onPressed: LoginUser),
                                      flex: 1,
                                    ),
                                  ],
                                ),
                                SizedBox(height: 15.0),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      '¿Nuevo?',
                                      style:
                                          TextStyle(fontFamily: 'Montserrat'),
                                    ),
                                    SizedBox(width: 5.0),
                                    GestureDetector(
                                      onTap: () {
                                        Navigator.of(context)
                                            .pushNamed('/signup');
                                      },
                                      child: InkWell(
                                        child: Text(
                                          'Registrate Aquí',
                                          style: TextStyle(
                                              color: Colors.blue,
                                              fontFamily: 'Montserrat',
                                              fontWeight: FontWeight.bold,
                                              decoration:
                                                  TextDecoration.underline),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                                // OutlineButton(
                                //     child: Text("signup"),
                                //     onPressed: () {
                                //       Navigator.of(context)
                                //           .pushNamed('/signup');
                                //     }),
                                // OutlineButton(
                                //     child: Text("ui"),
                                //     onPressed: () {
                                //       Navigator.of(context)
                                //           .pushNamed('/userpage');
                                //     })
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              )),
            ),
          ),
        ],
      ),
    );
  }

  Widget _input(String validation, bool, String label, String hint, save,String value) {
    return new TextFormField(
      decoration: InputDecoration(
        hintText: hint,
        labelText: label,
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 20.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
      ),
      obscureText: bool,
      initialValue: value,
      validator: (value) => value.isEmpty ? validation : null,
      onSaved: save,
    );
  }
}
